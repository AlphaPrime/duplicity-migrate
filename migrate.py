#!/bin/env python3

# Copyright (C) Maciej Suminski <orson@orson.net.pl>
# Kenneth Loafman <kenneth#loafman.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may find one here:
# https://www.gnu.org/licenses/gpl-3.0.html
# or you may search the http://www.gnu.org website for the version 3 license,
# or you may write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

# Script to migrate a Launchpad bug tracker to Gitlab issues

from launchpadlib.launchpad import Launchpad
import gitlab
import requests
import pickle
import datetime
import os
import re
import textwrap
from pprint import pprint
import config as cfg

re_user_reference = re.compile(r'(^|\s)@([^\s]+)')
re_build_info = re.compile(r'^((?:Application: .+)=(?:OFF|ON))$', re.DOTALL | re.MULTILINE)
re_bug_link = re.compile(r'((?:lp:#?)|(?:https?://bugs.launchpad.net/duplicity/\+bug/))([0-9]+)', re.MULTILINE)

def save_map(map_data, filename):
    with open(filename, 'wb') as f:
        pickle.dump(map_data, f, pickle.HIGHEST_PROTOCOL)


def load_map(filename):
    if not os.path.isfile(filename):
        self.log.warning('%s map does not exist' % filename)
        return

    with open(filename, 'rb') as f:
        return pickle.load(f)


wrapper = textwrap.TextWrapper(width=75, replace_whitespace=False,
        break_long_words=False)

def fix_text(text):
    lines = text.split('\n')
    for i, line in enumerate(lines):
        line = re_user_reference.sub(lambda match: '@ {0}'.format(match.group(2)), line)
        line = '\n'.join(wrapper.wrap(line))
        lines[i] = line

    text = '```\n' + '\n'.join(lines) + '\n```'

    return text


def fix_date(date):
    return date.isoformat().replace('+', '%2B')


def fix_tags(tags):
    fixed = []

    for t in tags:
        if t in cfg.tags_official:
            fixed.append(t)
        elif t in cfg.tags_map:
            fixed.append(cfg.tags_map[t])

    return list(set(fixed))


def importance_tag(importance):
    if importance == 'Critical':  return 'priority::critical'
    if importance == 'High':      return 'priority::high'
    if importance == 'Medium':    return 'priority::medium'
    if importance == 'Low':       return 'priority::low'
    if importance == 'Wishlist':  return 'priority::wishlist'
    if importance == 'Unknown':   return 'priority::undecided'
    if importance == 'Undecided': return 'priority::undecided'
    return None


def status_tag(status):
    if status == 'New':           return 'status::new'
    if status == 'Incomplete':    return 'status::need-info'
    if status == 'Opinion':       return 'status::opinion'
    if status == 'Invalid':       return 'status::as-designed'
    if status == 'Won\'t Fix':    return 'status::as-designed'
    if status == 'Expired':       return 'status::need-info'
    if status == 'Confirmed':     return 'status::confirmed'
    if status == 'Triaged':       return 'status::confirmed'
    if status == 'In Progress':   return 'status::in-progress'
    if status == 'Fix Committed': return 'status::fix-committed'
    if status == 'Fix Released':  return 'status::fix-released'
    #if status == 'Unknown':       return
    return None


def convert_status(status):
    if status == 'New':           return 'active'
    if status == 'Incomplete':    return 'active'
    if status == 'Opinion':       return 'active'
    if status == 'Invalid':       return 'close'
    if status == 'Won\'t Fix':    return 'close'
    if status == 'Expired':       return 'active'
    if status == 'Confirmed':     return 'active'
    if status == 'Triaged':       return 'active'
    if status == 'In Progress':   return 'active'
    if status == 'Fix Committed': return 'close'
    if status == 'Fix Released':  return 'close'
    return None


def is_closed(status):
    return convert_status(status) == 'close'


def get_milestone_id(project, name):
    if not name:
        return None

    milestone = project.milestones.list(title=name)

    if milestone:
        return milestone[0].id

    # does not exist, so create one
    milestone = project.milestones.create({'title': name})
    return milestone.id


def search_uid(gitlab, name):
    users = gitlab.users.list(search=name)
    return users[0].id if len(users) == 1 else None


def get_uid_by_login(gitlab, login):
    users = gitlab.users.list(username=login)
    return users[0].id if len(users) == 1 else None


def match_person(gitlab, person):
    if not person:
        return None

    if person.name in cfg.person_map:   # TODO add a cache for faster lookups
        return get_uid_by_login(gitlab, cfg.person_map[person.name])

    user = search_uid(gitlab, person.display_name)

    if user:
        return user

    user = search_uid(gitlab, person.name)

    if user:
        return user

    return None


def lp_link(number):
    return 'https://bugs.launchpad.net/bugs/{0}'.format(number)


def upload(target, name, file_path):
    try:
        uploaded = target.upload(name, filepath=file_path)
        return uploaded
    except gitlab.exceptions.GitlabUploadError as e:
        print('!! Could not upload file "%s"' % file_path)
        return None


def download(url, file_name):
    if not os.path.exists(cfg.tmp):
        os.mkdir(cfg.tmp)
    response = requests.get(url, stream=True)
    response.raise_for_status()
    download_path = os.path.join(cfg.tmp, file_name)

    with open(download_path, 'wb') as handle:
        for block in response.iter_content(1024):
            handle.write(block)

    return download_path


def transfer_attachments(target, collection):
    attachments = []

    for attachment in collection:
        trials = 5

        while trials:
            try:
                print('   Transferring attachment {0} ({1})'.format(attachment.title, attachment.data_link))
                file_path = download(attachment.data_link, attachment.title)
                uploaded = upload(target, attachment.title, file_path)
                attachments.append(uploaded)
                break
            except:
                print('!! Could not download file {0}'.format(attachment.data_link))
                trials -= 1

    return attachments


def delete_issues(project):
    for issue in project.issues.list(iterator=True):
        print(f'{issue.iid} ', end='')
        if 'lp:#' in issue.title:
            issue.delete()
            print('deleted')
        else:
            print('kept')


def has_lp_links(text):
    return re_bug_link.findall(text) != []


def fix_lp_links_func(match):
    bug_id = int(match.group(2))

    try:
        return '#{0}'.format(bug_map[bug_id])
    except:
        print('Could not fix LP link for bug #{0}'.format(bug_id))
        return match.group(0)


def fix_lp_links(text):
    return re_bug_link.sub(fix_lp_links_func, text)


def migrate_bug(bug_id):
    success = False

    while not success:
        try:
            print('Migrating bug report lp:#{0} ({1})...'.format(bug_id, lp_link(bug_id)))
            bug = lp.bugs[bug_id]
            task = bug.bug_tasks[0]

            try:
                assignee_id = task.assignee_link.split('~')[-1]
                assignee = lp.people[assignee_id]
            except:
                assignee = None

            try:
                milestone = task.milestone_link.split('/')[-1]
            except:
                milestone = None

            # original report reference
            description = '[Original report]({0}) '.format(lp_link(bug_id))
            description += 'created by **{0} ({1})**\n'.format(task.owner.display_name, task.owner.name)
            description += fix_text(bug.description)

            if bug.tags:
                description += '\nOriginal tags: {0}'.format(' '.join(bug.tags))

            issue_data = {
                'title': bug.title + ' (lp:#{0})'.format(bug_id),
                'description': description,
                'labels': [*fix_tags(bug.tags), importance_tag(task.importance), status_tag(task.status)],
                'created_at': fix_date(bug.date_created),
                'milestone_id': get_milestone_id(gl_project, milestone),
                'weight': bug.heat,
                'assignee_id': match_person(gl, assignee)
            }

            issue = gl_project.issues.create(issue_data) #, sudo=self._get_user_id(issues_row['author_id']))

            to_close = task.date_created < datetime.datetime(*cfg.cutoff_date,
                                                             tzinfo=datetime.timezone.utc)
            if to_close:
                print(f'   Bug #{bug_id} closed because it was created before version {cfg.cutoff_version}.')

            if is_closed(task.status) or to_close:
                issue.state_event = 'close'
                issue.save()

            if has_lp_links(bug.description):
                need_link_fix.append(issue)

            # comments
            first = True
            for msg in bug.messages:
                if first:
                    first = False   # first message contains the bug report
                    continue

                try:
                    if msg.content:
                        contents = '**{0} ({1})** wrote:\n'.format(msg.owner.display_name, msg.owner.name)
                        contents += fix_text(msg.content)
                    else:
                        contents = ''

                    attachments = transfer_attachments(gl_project, msg.bug_attachments_collection)

                    if attachments:
                        if contents:
                            contents += '\n**Attachments:**\n'
                        else:
                            contents = '**{0} ({1})** added attachment(s):\n'.format(msg.owner.display_name, msg.owner.name)

                        for att in attachments:
                            contents += '- {0}\n'.format(att['markdown'])

                    msg_data = {
                        'body': contents,
                        'created_at': fix_date(msg.date_created)
                    }

                    note = issue.notes.create(msg_data) #, sudo=self._get_user_id(journals_row['user_id']))

                    if has_lp_links(msg.content):
                        need_link_fix.append(note)
                except:
                    print('!! Could not transfer message {0} ({1})'.format(msg.web_link, msg.self_link))

            print('Migrated bug report lp:#{0} to issue #{1} (https://{2}/{3}/issues/{4})'.format(bug_id, issue.iid, cfg.gitlab_host, cfg.gitlab_project, issue.iid))
            bug_map[bug_id] = issue.iid
            new_bugs[bug_id] = issue.iid
            success = True

            #if idx == 250:   # TODO
            #    break
        except Exception as e:
            print(f'Error: "{str(e)}": retrying')
            pass


lp = Launchpad.login_with(cfg.launchpad_login, 'production',
        cfg.launchpad_cachedir, version='devel', credentials_file=cfg.launchpad_creds_file)
#lp = Launchpad.login_anonymously('production', version='devel')
gl = gitlab.Gitlab(cfg.gitlab_url, private_token=cfg.gitlab_token)

lp_project = lp.distributions[cfg.launchpad_project]
gl_project = gl.projects.get(cfg.gitlab_project)

if cfg.reset_issues:
    print(f'Deleting the existing issues from Gitlab project: {cfg.gitlab_project}')
    delete_issues(gl_project)
    bug_map = {}
else:
    try:
        bug_map = load_map(cfg.bugmap_file)
    except:
        bug_map = {}

new_bugs = {}

# issues and messages that need to have links adjusted
need_link_fix = []

# iterate through project's bugs on launchpad
#for idx, task in enumerate(map(lambda bug_id: lp.bugs[bug_id].bug_tasks[0],
#    [1831370], 1463510, 1466955, 1477591, 1477599, 1826682])):
count = 0
for idx, task in enumerate(lp_project.searchTasks()):
    bug_id = int(task.bug_link.split('/')[-1])
    migrate_bug(bug_id)
    count = count + 1

# when all issues all transferred, mapping between Launchpad bugs and Gitlab issues is complete
# now it is the time to fix links and set duplicates (related issues)
for lp_id, gl_id in list(new_bugs.items()):
    bug = lp.bugs[lp_id]
    issue = gl_project.issues.get(gl_id)

    # duplicates (linked/related issues in Gitlab)
    for duplicate in bug.duplicates:
        try:
            if not duplicate.id in bug_map:
                migrate_bug(duplicate.id)

            link_data = {
                'target_project_id': gl_project.id,
                'target_issue_iid': bug_map[duplicate.id]
            }
            issue.links.create(link_data)

            duplicate_issue = gl_project.issues.get(bug_map[duplicate.id])
            new_labels = list(filter(lambda x: not 'status::' in x, duplicate_issue.labels))
            new_labels.append('status::duplicate')
            duplicate_issue.labels = new_labels
            duplicate_issue.state_event = 'close'
            duplicate_issue.save()

            print('Added a duplicate ({0} duplicates {1})'.format(bug_map[duplicate.id], gl_id))
        except gitlab.exceptions.GitlabHttpError as e:
            print(e)
        except gitlab.exceptions.GitlabCreateError as e:
            print(e)
        except:
            print('Could not find a Gitlab issue duplicate '
                  '(original: lp:#{0}, Gitlab: {1}; duplicate: lp:#{2})'.format(
                    lp_id, gl_id, duplicate.id))

for entry in need_link_fix:
    try:
        if type(entry) == 'gitlab.v4.objects.ProjectIssueNote':
            entry.body = fix_lp_links(entry.body)
        elif type(entry) == 'gitlab.v4.objects.Issue':
            entry.description = fix_lp_links(entry.description)

        entry.save()
    except:
        print("!! Could not fix LP link in ", entry)

print('>>> Transferred {0} bugs'.format(count))
save_map(bug_map, cfg.bugmap_file)
